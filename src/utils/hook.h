#ifndef __MINHOOK_HOOK_H__
#define __MINHOOK_HOOK_H__

#include "MinHook.h"

#include <map>
#include <locale>
#include <codecvt>
#include <cstdint>
#include <optional>
#include <string_view>

#define REQUIRE_OK(FUNC)     \
    {                        \
        MH_STATUS _s = FUNC; \
        if(_s != MH_OK)      \
            return _s;       \
    }

using addr_t = std::uintptr_t;
namespace Hook {

    inline MH_STATUS init() {
        return MH_Initialize();
    }

    extern std::map<LPVOID, LPVOID> hook_book;
    extern std::map<std::string, LPVOID> name_to_hook;

    /**
     * @brief 安装 hook
     *
     * @tparam T
     * @param module_name dll 的名称
     * @param proc_name 函数的名称
     * @param hook_func 用来替换目标函数的函数
     * @return MH_STATUS
     */
    template<class T>
    MH_STATUS install(std::string_view module_name,
                      std::string_view proc_name,
                      T hook_func,
                      std::optional<std::string_view> name) {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> cover;
        auto w_module = cover.from_bytes(module_name.begin(), module_name.end());
        auto _proc_name = std::string(proc_name);
        auto hook = reinterpret_cast<LPVOID>(hook_func);
        LPVOID origin;

        REQUIRE_OK(MH_CreateHookApi(w_module.c_str(), _proc_name.c_str(), hook, &origin));
        REQUIRE_OK(MH_EnableHook(origin));
        hook_book[hook] = origin;

        if(name) {
            name_to_hook[std::string(name.value())] = origin;
        }

        return MH_OK;
    }

    template<class T>
    MH_STATUS install(addr_t address, T hook_func, std::optional<std::string_view> name) {
        auto hook = reinterpret_cast<LPVOID>(hook_func);
        LPVOID origin;
        auto target = reinterpret_cast<LPVOID>(address);

        REQUIRE_OK(MH_CreateHook(target, hook, &origin));
        REQUIRE_OK(MH_EnableHook(origin));
        hook_book[hook] = origin;

        if(name) {
            name_to_hook[std::string(name.value())] = origin;
        }

        return MH_OK;
    }

    template<class T>
    T call(std::string_view name){
        std::string _name(name);
        if (name_to_hook.find(_name) != name_to_hook.end()) {
            return reinterpret_cast<T>(name_to_hook.at(_name));
        }
        return nullptr;
    }

    template<class T>
    T call(T func) {
        return reinterpret_cast<T>(hook_book.at(func));
    }

} // namespace Hook

#endif