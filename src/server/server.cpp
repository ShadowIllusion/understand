#include "httplib.h"

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/bio.h>
#include <openssl/asn1.h>
#include <map>

#include "message"

static void solve(const httplib::Request &req, httplib::Response &res) {
    static std::string content_type = "application/json";
    static std::map<std::string, std::string> actions = {
        { "areYouThere", areYouThere }, { "findMessage", findMessage }, { "testLicense", testLicense },
        { "findLicense", license },     { "register", license },        { "quit", quit }
    };

    auto action = req.path_params.at("action");

    printf("-> /%s from %s:%d\n", action.c_str(), req.remote_addr.c_str(), req.remote_port);

    if(actions.find(action) == actions.end()) {
        action.clear();
    }
    res.set_content(actions[action], content_type);
}

int main(void) {

    httplib::SSLServer svr([](SSL_CTX &ctx) {
        auto x509 = X509_new();
        auto pkey = EVP_RSA_gen(2048);
        ASN1_INTEGER_set(X509_get_serialNumber(x509), 1);
        X509_gmtime_adj(X509_get_notBefore(x509), 0);
        X509_gmtime_adj(X509_get_notAfter(x509), 31536000L);
        X509_set_pubkey(x509, pkey);

        X509_NAME *name = X509_get_subject_name(x509);
        X509_NAME_add_entry_by_txt(name, "C", MBSTRING_ASC, (const unsigned char *)"CN", -1, -1, 0);
        X509_NAME_add_entry_by_txt(name, "O", MBSTRING_ASC, (const unsigned char *)"xuke", -1, -1, 0);
        X509_NAME_add_entry_by_txt(name, "OU", MBSTRING_ASC, (const unsigned char *)"xuke", -1, -1, 0);
        X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC, (const unsigned char *)"127.0.0.1", -1, -1, 0);
        X509_set_issuer_name(x509, name);
        X509_sign(x509, pkey, EVP_sha256());
        SSL_CTX_use_cert_and_key(&ctx, x509, pkey, nullptr, 1);

        EVP_PKEY_free(pkey);
        X509_free(x509);
        return true;
    });

    svr.Post("/api/v1/:action", solve);

    svr.listen("0.0.0.0", 443);

}


