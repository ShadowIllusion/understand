#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <windows.h>
#include "../utils/hook.h"

#include "MinHook.h"

typedef int (*orig_getaddrinfo_type)(const char *node,
                                     const char *service,
                                     const struct addrinfo *hints,
                                     struct addrinfo **res);
static orig_getaddrinfo_type orig_getaddrinfo = NULL;

int getaddrinfo(const char *node, const char *service, const struct addrinfo *hints, struct addrinfo **res) {
    if(node && strcmp(node, "licensing.scitools.com") == 0) {
        const char *fake_host = "localhost";
        return orig_getaddrinfo(fake_host, service, hints, res);
    } else {
        return orig_getaddrinfo(node, service, hints, res);
    }
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {

    switch(fdwReason) {
        case DLL_PROCESS_ATTACH: {
            MH_Initialize();
            
            MH_CreateHookApi(L"getaddrinfo", "ws2_32", reinterpret_cast<LPVOID>(getaddrinfo),
                             reinterpret_cast<LPVOID *>(orig_getaddrinfo));
            MH_EnableHook(reinterpret_cast<LPVOID>(orig_getaddrinfo));
        } break;

        case DLL_THREAD_ATTACH:
            break;

        case DLL_THREAD_DETACH:
            break;

        case DLL_PROCESS_DETACH:
            break;
    }
    return TRUE;
}
